# Gatekeeper Policies

This repository provides a Helm chart that defines various Gatekeeper policies for the Common Platform.
This chart is separated from the landlord that uses the policies because of the need to define the
`ConstraintTemplate` objects and for those to be processed to create their corresponding CRDs. Only then
will the landlord successfully deploy.

## Installation

If you wish to deploy these policies in your own cluster, you can do so using the published charts.
You first need Gatekeeper installed.

```shell
helm repo add gatekeeper-policies https://code.vt.edu/api/v4/projects/7857/packages/helm/stable
helm repo update
helm install gatekeeper-policies gatekeeper-policies/constraint-templates
```

## Repo Structure

To better support testing of the rego scripts, all rego is externalized from the Helm templates and placed within the `rego/` directory. Each policy has its own `policy.rego` file and a corresponding `policy_test.rego`.

## Policy List

The following policies are defined within this chart:

- `AuthorizedDomainPolicy` - provides the ability to define the hostnames that a namespace is authorized to use. These are currently being tested against Ingress and Certificate objects
- `BlockLoadBalancer` - prevents LoadBalancer Services from being defined
- `BlockNodePort` - prevents NodePort Services from being defined
- `ContainerResourceQuotas` - requires CPU/memory definitions for resource requests and limits
- `FluxTenant` - ensures the `serviceAccountName` and `targetNamespace` fields are specified and that the `targetNamespace` matches the namespace on Kustomization and HelmRelease objects, preventing namespace escapes


## Development

To write a new policy, use the following steps:

1. Create a sub-directory in the `rego/` directory for the new policy, following the existing naming convention of snake casing.
1. Create a `policy.rego` and `policy_test.rego` in the new directory.
1. Create the policy and write appropriate unit tests (feel free to reference those in other policies)
1. You can test the policies using the following command:

    ```shell
    docker run -w /src -v $PWD:/src openpolicyagent/opa test ./rego -v
    ```

1. In the `templates/` directory, copy one of the existing ConstraintTemplate YAML files and make a new one, matching the name of the rego policy directory created earlier.
1. In the new `ConstraintTemplate`, replace the names to match the new policy name, as well as the path to the rego policy that should be inlined.

### Tips for Writing Policies

A few tips for writing policies:

- **Write your tests first!** Write both positive and negative tests and then
  write the policy until all of your tests pass.
- **Validate the object kind in all policies.** Although Gatekeeper allows us
  to configure what objects will match a policy, we want to make sure config
  errors don't cause unintended side-effects. 
