package k8simageprovenance
# LLM-GPT4.0
# Helper function to create mock input for a Pod with different types of container images
create_input(image, container_type) = {
    "review": {
        "object": {
            "kind": "Pod",
            "spec": {
                container_type: [
                    {
                        "image": image
                    }
                ]
            }
        }
    }
}

# Test with an allowed image repository
test_with_allowed_image_repository {
    container_types := ["containers", "initContainers", "ephemeralContainers"]
    container_type := container_types[_]
    test_input := create_input("docker.io/nginx", container_type)
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 0
}

# Test with a disallowed image repository
test_with_disallowed_image_repository {
    container_types := ["containers", "initContainers", "ephemeralContainers"]
    container_type := container_types[_]
    test_input := create_input("unlistedrepo.com/image", container_type)
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 1
}

# Test with no repository specified (defaults to Docker Hub)
test_with_default_docker_hub_image {
    container_types := ["containers", "initContainers", "ephemeralContainers"]
    container_type := container_types[_]
    test_input := create_input("nginx", container_type)
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 0
}

# Test with multiple containers, mixed repositories
test_with_mixed_repositories {
    test_input := {
        "review": {
            "object": {
                "kind": "Pod",
                "spec": {
                    "containers": [
                        {"image": "docker.io/nginx"},
                        {"image": "unlistedrepo.com/image"}
                    ],
                    "initContainers": [
                        {"image": "docker.io/initnginx"}
                    ],
                    "ephemeralContainers": [
                        {"image": "quay.io/tempnginx"}
                    ]
                }
            }
        }
    }
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 1
}

# Test with image from Docker Hub's default repository
test_with_docker_hub_default_repository {
    container_types := ["containers", "initContainers", "ephemeralContainers"]
    container_type := container_types[_]
    test_input := create_input("alpine", container_type)
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 0
}

# Test with image from non-standard repository path
test_with_non_standard_repository_path {
    container_types := ["containers", "initContainers", "ephemeralContainers"]
    container_type := container_types[_]
    test_input := create_input("docker.io/somepath/nginx", container_type)
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 0
}

# Separate tests for each container type with allowed images
test_with_allowed_image_containers {
    test_input := create_input("docker.io/nginx", "containers")
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 0
}

test_with_allowed_image_init_containers {
    test_input := create_input("docker.io/nginx", "initContainers")
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 0
}

test_with_allowed_image_ephemeral_containers {
    test_input := create_input("docker.io/nginx", "ephemeralContainers")
    results := data.k8simageprovenance.violation with input as test_input
    count(results) == 0
}
