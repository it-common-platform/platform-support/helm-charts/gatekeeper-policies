package allow_rollout_restart_statefulset

test_with_statefulset_and_correct_annotation {
  input := review_statefulset_patch_existing("kubectl.kubernetes.io/restartedAt","14oclock","randomuser","randomgroup","13oclock")

  results := violation
    with input as input

  count(results) == 0
}

test_with_statefulset_and_incorrect_annotation {
  input := review_statefulset_patch_existing("wrongAnnotation","wrong","randomuser","randomgroup","13oclock")

  results := violation
    with input as input

  count(results) == 1
}

test_with_statefulset_and_incorrect_annotation_with_flux {
  input := review_statefulset_patch_existing("wrongAnnotation","wrong","system:serviceaccount:test:flux","randomgroup","13oclock")

  results := violation
    with input as input

  count(results) == 0
}

test_with_statefulset_and_incorrect_annotation_with_stakater {
  input := review_statefulset_patch_existing("wrongAnnotation","wrong","system:serviceaccount:platform-stakater-reloader:stakater-reloader","randomgroup","13oclock")

  results := violation
    with input as input

  count(results) == 0
}

test_with_statefulset_and_incorrect_annotation_with_platform_admin_group {
  input := review_statefulset_patch_existing("wrongAnnotation","wrong","randomuser","oidc:it.platform.roles.admin","13oclock")

  results := violation
    with input as input

  count(results) == 0
}

test_with_statefulset_and_too_many_annotations{
  input := review_statefulset_patch_extra_annotations("kubectl.kubernetes.io/restartedAt","14oclock","randomuser","randomgroup","13oclock")

  results := violation
    with input as input

  count(results) == 1
}

test_with_statefulset_and_correct_annotation_no_existing_annotation {
  input := review_statefulset_patch_absent("kubectl.kubernetes.io/restartedAt","14oclock","randomuser","randomgroup")

  results := violation
    with input as input

  count(results) == 0
}

test_with_statefulset_and_incorrect_annotation_no_existing_annotation {
  input := review_statefulset_patch_absent("wrongAnnotation","wrong","randomuser","randomgroup")

  results := violation
    with input as input

  count(results) == 1
}

review_statefulset_patch_extra_annotations(annotationname,annotationvalue,username,groupname,previousrestarted) = out {
  out = {
    "review": {
      "namespace": "test",
      "userInfo": {
        "username": username,
        "groups": [ groupname ]
      },
      "operation": "UPDATE",
      "kind": {
        "kind": "StatefulSet"
      },
      "object": {
        "spec": {
          "template": {
            "metadata": {
              "annotations": {
                  "dreaded_gazebo": "indeed",
                  annotationname: annotationvalue
              }
            }
          }
        }
      },
      "oldObject": {
        "spec": {
          "template": {
            "metadata": {
              "annotations": {
                "kubectl.kubernetes.io/restartedAt": previousrestarted
              }
            } 
          }
        }
      }
    }
  }
}

review_statefulset_patch_existing(annotationname,annotationvalue,username,groupname,previousrestarted) = out {
  out = {
    "review": {
      "namespace": "test",
      "userInfo": {
        "username": username,
        "groups": [ groupname ]
      },
      "operation": "UPDATE",
      "kind": {
        "kind": "StatefulSet"
      },
      "object": {
        "spec": {
          "template": {
            "metadata": {
              "annotations": {
                  annotationname: annotationvalue
              }
            }
          }
        }
      },
      "oldObject": {
        "spec": {
          "template": {
            "metadata": {
              "annotations": {
                "kubectl.kubernetes.io/restartedAt": previousrestarted
              }
            }
          }
        }
      }
    }
  }
}

review_statefulset_patch_absent(annotationname,annotationvalue,username,groupname) = out {
  out = {
    "review": {
      "namespace": "test",
      "userInfo": {
        "username": username,
        "groups": [ groupname ]
      },
      "operation": "UPDATE",
      "kind": {
        "kind": "StatefulSet"
      },
      "oldObject": {
        "spec": {
          "template": {
            "metadata": {
              "annotations": {}
            }
          }
        }
      }, 
      "object": {
        "spec": {
          "template": {
            "metadata": {
                "annotations": {
                    annotationname: annotationvalue
                }
            }
          }
        }
      }
    }
  }
}

