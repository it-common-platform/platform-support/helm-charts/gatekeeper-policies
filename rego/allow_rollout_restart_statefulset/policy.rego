package allow_rollout_restart_statefulset

# Violation caused when user is not local flux, not the stakater-reloader, and not in the admin group, trying to use UPDATE to change a StatefulSet. The old annotations get patched with a value to build the datastructure, but it gets removed in the next step, so the value is innocuous.
violation[{"msg": msg}] {
    not input.review.userInfo.username == "system:serviceaccount:platform-stakater-reloader:stakater-reloader"
    not input.review.userInfo.username == "system:serviceaccount:platform-prometheus-stack:prometheus-kube-prometheus-operator"
    flux_username := concat("",["system:serviceaccount:",input.review.namespace,":flux"])
    not input.review.userInfo.username == flux_username
    not contains(input.review.userInfo.groups, "oidc:it.platform.roles.admin")
    input.review.kind.kind == "StatefulSet"
    input.review.operation == "UPDATE"
    new_annotations := json.remove(input.review.object,["spec/template/metadata/annotations/kubectl.kubernetes.io~1restartedAt","metadata/generation","metadata/managedFields"])
    old_patched_annotations := json.patch(input.review.oldObject,[{"op": "add", "path": "spec/template/metadata/annotations/kubectl.kubernetes.io~1restartedAt", "value": 3}])
    old_annotations := json.remove(old_patched_annotations,["spec/template/metadata/annotations/kubectl.kubernetes.io~1restartedAt","metadata/generation","metadata/managedFields"])
    old_annotations != new_annotations
	msg := sprintf("Only patch to annotation kubectl.kubernetes.io/restartedAt is allowed.",[])

}


contains(grouparray,testgroup) {
    grouparray[_] = testgroup
}