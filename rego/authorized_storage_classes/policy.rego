package pvcstorageclassmatchesnodepool

violation[{"msg": msg}] {
  input.review.kind.kind == "PersistentVolumeClaim"
#  input.review.operation == "CREATE"
  
  not input.review.object.spec.storageClassName == ""
  pvc_sc_name := input.review.object.spec.storageClassName
  not input.review.object.spec.volumeName
  not data.inventory.cluster["storage.k8s.io/v1"]["StorageClass"][pvc_sc_name].metadata.labels["platform.it.vt.edu/node-pool"]
  
  msg := sprintf("PVC storage class '%v' is not allowed missing node pool label, contact administrator", [pvc_sc_name])
}

# Violation caused by PVCs, if they reference a volume name and storage class name is blank, ignoring those that reference pv with node-pool label
violation[{"msg": msg}] {
  input.review.kind.kind == "PersistentVolumeClaim"

  input.review.object.spec.volumeName 
  pvc_pv_name := input.review.object.spec.volumeName
  input.review.object.spec.storageClassName == ""
  not data.inventory.cluster["v1"]["PersistentVolume"][pvc_pv_name].metadata.labels["platform.it.vt.edu/node-pool"]
  
  msg := sprintf("PVC volume name '%v' is not allowed missing node pool label, contact administrator", [pvc_pv_name])
}
# This violation is triggered by having both a volume name and a storage class name other than blank specified
violation[{"msg": msg}] {
  input.review.kind.kind == "PersistentVolumeClaim"
  input.review.operation == "CREATE"

  input.review.object.spec.volumeName 
  input.review.object.spec.storageClassName

  pvc_pv_name := input.review.object.spec.volumeName
  pvc_sc_name := input.review.object.spec.storageClassName

  pvc_sc_name != ""
  
  msg := sprintf("PVC volume name '%v' is not allowed storage class '%v' not allowed, contact administrator", [pvc_pv_name, pvc_sc_name])
}

violation[{"msg": msg}] {
  input.review.kind.kind == "PersistentVolumeClaim"
  
  input.review.object.metadata.namespace

  pvc_namespace := input.review.object.metadata.namespace
  not data.inventory.cluster["v1"]["Namespace"][pvc_namespace].metadata.labels["platform.it.vt.edu/node-pool"]

  msg := sprintf("PVC namespace '%v' is not allowed missing node pool label, contact administrator", [pvc_namespace])
}

violation[{"msg": msg}] {
  input.review.kind.kind == "PersistentVolumeClaim"

  input.review.object.spec.storageClassName
  input.review.object.metadata.namespace

  pvc_sc_name := input.review.object.spec.storageClassName
  pvc_namespace := input.review.object.metadata.namespace
  sc_node_pool := data.inventory.cluster["storage.k8s.io/v1"]["StorageClass"][pvc_sc_name].metadata.labels["platform.it.vt.edu/node-pool"]
  namespace_node_pool := data.inventory.cluster["v1"]["Namespace"][pvc_namespace].metadata.labels["platform.it.vt.edu/node-pool"]

  pvc_sc_name != ""
  sc_node_pool != namespace_node_pool

  msg := sprintf("PVC storage class '%v' is not allowed for node pool '%v', contact administrator", [pvc_sc_name, namespace_node_pool])
}

# Violation caused by PVCs with volume name and a namespace that have a nodepool label on pv that do not matches the pvc's namespace nodepool label.
violation[{"msg": msg}] {
  input.review.kind.kind == "PersistentVolumeClaim"
  
  input.review.object.spec.volumeName
  input.review.object.metadata.namespace

  pvc_pv_name := input.review.object.spec.volumeName
  pvc_namespace := input.review.object.metadata.namespace
  pv_node_pool := data.inventory.cluster["v1"]["PersistentVolume"][pvc_pv_name].metadata.labels["platform.it.vt.edu/node-pool"]
  namespace_node_pool := data.inventory.cluster["v1"]["Namespace"][pvc_namespace].metadata.labels["platform.it.vt.edu/node-pool"]

  pv_node_pool != namespace_node_pool
  
  msg := sprintf("PVC volume name '%v' is not allowed for node pool '%v', contact administrator", [pvc_pv_name, namespace_node_pool])
}