package pvcstorageclassmatchesnodepool

test_with_correct_nodepool {
  input := review_pvc("ebs-dvlp-test-delete", "mikesirtest")

  inventory := inventory_np("ebs-dvlp-test-delete","dvlp-test","mikesirtest","dvlp-test")

  results := violation 
    with data.inventory as inventory
    with input as input

  count(results) == 0
}

test_with_incorrect_nodepool {
  input := review_pvc("ebs-itso-delete", "mikesirtest")

  inventory := inventory_np("ebs-itso-delete","itso","mikesirtest","dvlp-test")

  results := violation 
    with data.inventory as inventory
    with input as input

  count(results) == 1
}

test_wrong_namespace {
  input := review_pvc("ebs-dvlp-test-delete", "mikesir-test")
  inventory := inventory_np("ebs-dvlp-test-delete", "dvlp-test", "mikesir-test", "wrong_nodepool")

  results := violation
  with data.inventory as inventory
  with input as input

  count(results) == 1
}

test_wrong_storageclass {
  input := review_pvc("ebs-dvlp-test-delete", "mikesir-test")
  inventory := inventory_np("ebs-dvlp-test-delete", "wrong_nodepool", "mikesir-test", "dvlp-test")

  results := violation
  with data.inventory as inventory
  with input as input

  count(results) == 1
}

test_missing_storageclass_nodepool {
  input := review_pvc("ebs-dvlp-test-delete", "mikesir-test")
  inventory := inventory_np("ebs-dvlp-test-delete", null, "mikesir-test", "wrong_nodepool")

  results := violation
  with data.inventory as inventory
  with input as input

  count(results) == 1
}

test_missing_namespace_nodepool {
  input := review_pvc("ebs-dvlp-test-delete", "mikesir-test")
  inventory := inventory_np("ebs-dvlp-test-delete", "wrong_nodepool", "mikesir-test", null)

  results := violation
  with data.inventory as inventory
  with input as input

  count(results) == 1
}

test_pv_with_incorrect_nodepool {
  input := review_pvc_pv("smb-mike-sir","","mikesir-test")
  inventory := inventory_np_pv("smb-mike-sir","dvlp-test", "mikesir-test", "dvlp-wrong")

  results := violation
  with data.inventory as inventory
  with input as input

  count(results) == 1
}

test_pv_with_correct_nodepool {
  input := review_pvc_pv("smb-mike-sir","","mikesir-test")
  inventory := inventory_np_pv("smb-mike-sir","dvlp-test", "mikesir-test", "dvlp-test")

  results := violation
  with data.inventory as inventory
  with input as input

  count(results) == 0
}

test_missing_pv_nodepool {
  input := review_pvc_pv("smb-mike-sir","", "mikesir-test")
  inventory := inventory_np_pv("smb-mike-sir", null, "mikesir-test", "dvlp-test")

  results := violation
  with data.inventory as inventory
  with input as input

  count(results) == 1
}

test_pv_with_sc_null {
  input := review_pvc_pv("smb-mike-sir", null, "mikesir-test")
  inventory := inventory_np_pv("smb-mike-sir", "dvlp-test", "mikesir-test", "dvlp-test")

  results := violation
  with data.inventory as inventory
  with input as input

  print(results)
  count(results) == 1
}

test_pv_and_sc {
  input := review_pvc_pv("smb-mike-sir","itso", "mikesir-test")
  inventory := inventory_np_pv_sc("smb-mike-sir", "dvlp-test", "itso", "dvlp-test", "mikesir-test", "dvlp-test")

  results := violation
  with data.inventory as inventory
  with input as input
  
  print(results)
  count(results) == 1
}

review_pvc(scname,namespace) = out {
  out = {
    "review": {
      "operation": "CREATE",
      "kind": {
        "kind": "PersistentVolumeClaim"
      },
      "object": {
        "spec": {
          "storageClassName": scname
        },
        "metadata": {
          "namespace": namespace
        }
      }
    }
  }
}

review_pvc_pv(pvname, scname, namespace) = out {
  out = {
    "review": {
      "operation": "CREATE",
      "kind": {
        "kind": "PersistentVolumeClaim"
      },
      "object": {
        "spec": {
          "volumeName": pvname,
          "storageClassName": scname
        },
        "metadata": {
          "namespace": namespace
        }
      }
    }
  }
}


inventory_np(scname,sclabel,namespace,nslabel) = out {
  out = {
    "cluster": {
      "storage.k8s.io/v1": {
        "StorageClass": {
          scname: {
            "metadata": {
              "labels": {
                "platform.it.vt.edu/node-pool": sclabel
              }
            }
          }
        }
      },
      "v1": {
        "Namespace": {
          namespace: {
            "metadata": {
              "labels": {
                "platform.it.vt.edu/node-pool": nslabel
              }
            }
          }
        }
      }
    }    
  }
}

inventory_np_pv(pvname,pvlabel,namespace,nslabel) = out {
  out = {
    "cluster": {
      "v1": {
        "PersistentVolume": {
          pvname: {
            "metadata": {
              "labels": {
                "platform.it.vt.edu/node-pool": pvlabel
              }
            }
          }
        },
        "Namespace": {
          namespace: {
            "metadata": {
              "labels": {
                "platform.it.vt.edu/node-pool": nslabel
              }
            }
          }
        }
      }
    }    
  }
}

inventory_np_pv_sc(pvname,pvlabel,scname,sclabel,namespace,nslabel) = out {
  out = {
    "cluster": {
      "storage.k8s.io/v1": {
        "StorageClass": {
          scname: {
            "metadata": {
              "labels": {
                "platform.it.vt.edu/node-pool": sclabel
              }
            }
          }
        }
      },
      "v1": {
        "PersistentVolume": {
          pvname: {
            "metadata": {
              "labels": {
                "platform.it.vt.edu/node-pool": pvlabel
              }
            }
          }
        },
        "Namespace": {
          namespace: {
            "metadata": {
              "labels": {
                "platform.it.vt.edu/node-pool": nslabel
              }
            }
          }
        }
      }
    }    
  }
}