package block_nodeports

test_with_nodeports {
  input := {"review": {"kind": {"kind": "Service"}, "object":{"spec": {"type": "NodePort"}, "metadata":{"name": "test"}}}}
  results := violation with input as input
  count(results) == 1
  }

test_without_nodeports {
  input := {"review": {"kind": {"kind": "Service"}, "object":{"metadata":{"name": "test"}}}}
  results := violation with input as input
  count(results) == 0
}