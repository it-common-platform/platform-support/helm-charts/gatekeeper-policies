package block_nodeports

violation[{"msg": msg}] {
	input.review.kind.kind == "Service"
	input.review.object.spec.type == "NodePort"
	msg := sprintf("NodePort not permitted on Service - %v", [input.review.object.metadata.name])
}