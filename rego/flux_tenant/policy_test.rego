package fluxtenant

test_helm_release_without_sa {
  spec := { "targetNamespace": "test" }
  input := sa_input_obj("helm.toolkit.fluxcd.io", "HelmRelease", spec)

  results := violation with input as input
  count(results) == 1
}

test_kustomization_without_sa {
  spec := { "targetNamespace": "test" }
  input := sa_input_obj("kustomize.toolkit.fluxcd.io", "Kustomization", spec)

  results := violation with input as input
  count(results) == 1
}

test_helm_release_with_nonmatching_targetNamespace {
  spec := { "targetNamespace": "unknown", "serviceAccountName": "test" }
  input := sa_input_obj("helm.toolkit.fluxcd.io", "HelmRelease", spec)

  results := violation with input as input
  count(results) == 1
}

test_kustomization_with_nonmatching_targetNamespace {
  spec := { "targetNamespace": "unknown", "serviceAccountName": "test" }
  input := sa_input_obj("kustomize.toolkit.fluxcd.io", "Kustomization", spec)

  results := violation with input as input
  count(results) == 1
}

test_helm_release_with_sa_and_matching_namespace {
  spec := { "targetNamespace": "test", "serviceAccountName": "test" }
  input := sa_input_obj("helm.toolkit.fluxcd.io", "HelmRelease", spec)

  results := violation with input as input
  count(results) == 0
}

test_kustomization_with_sa_and_matching_namespace {
  spec := { "targetNamespace": "test", "serviceAccountName": "test" }
  input := sa_input_obj("kustomize.toolkit.fluxcd.io", "Kustomization", spec)

  results := violation with input as input
  count(results) == 0
}

sa_input_obj(group, kind, spec) = out {
  out = {
    "review": {
      "kind": {
        "group": group,
        "kind": kind
      },
      "object": {
        "metadata": {
          "name": "test",
          "namespace": "test"
        },
        "spec": spec
      }
    },
    "parameters": {}
  }
}
