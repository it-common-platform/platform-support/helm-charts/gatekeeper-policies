package fluxtenant

violation[{"msg": msg}] {
  is_flux_applier(input.review.kind.group, input.review.kind.kind)
  not input.review.object.spec.serviceAccountName
  msg := sprintf("serviceAccountName missing from HelmRelease %v", [input.review.object.metadata.name])
}

violation[{"msg": msg}] {
  is_flux_applier(input.review.kind.group, input.review.kind.kind)
  not input.review.object.metadata.namespace == input.review.object.spec.targetNamespace
  msg := sprintf("targetNamespace must be specified and match the namespace", [])
}

is_flux_applier(group, kind) {
  group == "helm.toolkit.fluxcd.io"
  kind == "HelmRelease"
}

is_flux_applier(group, kind) {
  group == "kustomize.toolkit.fluxcd.io"
  kind == "Kustomization"
}
