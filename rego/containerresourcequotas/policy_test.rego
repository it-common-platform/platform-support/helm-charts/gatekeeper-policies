package containerresourcequotas

import data.containerresourcequotas

test_pod_without_cpu_request {
  resources_spec := {"resources":{"requests":{"memory":"64Mi"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, pod_definition(resources_spec))
  results := violation with input as input_object
  count(results) != 0
}

test_deployment_without_cpu_request {
  resources_spec := {"resources":{"requests":{"memory":"64Mi"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, deployment_definition(resources_spec))
  results := violation with input as input_object
  count(results) != 0
}

test_pod_without_memory_request {
  resources_spec := {"resources":{"requests":{"cpu":"250m"}}}
  parameters := {"cpu":"4","memory":"4096Mi"}
  input_object := input_obj(parameters, pod_definition(resources_spec))
  results := violation with input as input_object
  count(results) != 0
}

test_deployment_without_memory_request {
  resources_spec := {"resources":{"requests":{"cpu":"250m"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, deployment_definition(resources_spec))
  results := violation with input as input_object
  count(results) != 0
}

test_pod_with_requests {
  resources_spec := {"resources":{"requests":{"memory":"64Mi","cpu":"250m"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, pod_definition(resources_spec))
  results := violation with input as input_object
  count(results) == 0
}

test_deployment_with_requests {
  resources_spec := {"resources":{"requests":{"memory":"64Mi","cpu":"250m"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, deployment_definition(resources_spec))
  results := violation with input as input_object
  count(results) == 0
}

test_pod_without_resources {
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, pod_definition_without_resources)
  results := violation with input as input_object
  count(results) != 0
}

test_deployment_without_resources {
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, deployment_definition_without_resources)
  results := violation with input as input_object
  count(results) != 0
}

test_pod_with_requests_and_limits {
  resources_spec := {"resources":{"requests":{"memory":"64Mi","cpu":"250m"},"limits":{"memory":"128Mi","cpu":"500m"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, pod_definition(resources_spec))
  results := violation with input as input_object
  count(results) == 0
}

test_deployment_with_requests_and_limits {
  resources_spec := {"resources":{"requests":{"memory":"64Mi","cpu":"250m"},"limits":{"memory":"128Mi","cpu":"500m"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, deployment_definition(resources_spec))
  results := violation with input as input_object
  count(results) == 0
}

test_pod_over_max_limits {
  resources_spec := {"resources":{"requests":{"memory":"64Mi","cpu":"250m"},"limits":{"memory":"8192Mi","cpu":"8"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, pod_definition(resources_spec))
  results := violation with input as input_object
  count(results) != 0
}

test_deployment_over_max_limits {
  resources_spec := {"resources":{"requests":{"memory":"64Mi","cpu":"250m"},"limits":{"memory":"8192Mi","cpu":"8"}}}
  parameters := {"cpu": "4", "memory":"4096Mi"}
  input_object := input_obj(parameters, deployment_definition(resources_spec))
  results := violation with input as input_object
  count(results) != 0
}

pod_definition(container_resources_spec) = out {
  out = {
    "object": {
      "apiVersion": "v1",
      "kind": "Pod",
      "metadata": {
        "name": "test-pod"
      },
      "spec": {
        "containers": [
          {
            "name": "test-app",
            "image": "images.my-company.example/app:v4",
            "resources": container_resources_spec.resources
          }
        ]
      }
    }
  }
}

pod_definition_without_resources = out {
  out = {
    "object": {
      "apiVersion": "v1",
      "kind": "Pod",
      "metadata": {
        "name": "test-pod"
      },
      "spec": {
        "containers": [
          {
            "name": "test-app",
            "image": "images.my-company.example/app:v4"
          }
        ]
      }
    }
  }
}

deployment_definition(container_resources_spec) = out {
  out = {
    "object": {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
         "name": "test-deployment"
      },
      "spec": {
        "replicas": 2,
        "selector": {
          "matchLabels": {
            "name": "test-deployment"
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "name": "test-deployment"
            }
          },
          "spec": {
            "containers": [
              {
                "name": "test-app",
                "image": "images.my-company.example/app:v4",
                "resources": container_resources_spec.resources
              }
            ]
          }
        }
      }
    }
  }
}

deployment_definition_without_resources = out {
  out = {
    "object": {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
         "name": "test-deployment"
      },
      "spec": {
        "replicas": 2,
        "selector": {
          "matchLabels": {
            "name": "test-deployment"
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "name": "test-deployment"
            }
          },
          "spec": {
            "containers": [
              {
                "name": "test-app",
                "image": "images.my-company.example/app:v4"
              }
            ]
          }
        }
      }
    }
  }
}

input_obj(parameters, review) = out {
  out = {
    "parameters": parameters,
    "review": review
  }
}
