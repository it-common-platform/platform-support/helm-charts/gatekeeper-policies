package block_loadbalancers

test_with_loadbalancer {
  input := {"review": {"kind": {"kind": "Service"}, "object": {"spec": {"type": "LoadBalancer"}, "metadata":{"name": "test"}}}}
  results := violation with input as input
  count(results) == 1
}

test_without_loadbalancer {
  input := {"review": {"kind": {"kind": "Service"}, "object": {"metadata":{"name": "test"}}}}
  results := violation with input as input
  count(results) == 0
  }