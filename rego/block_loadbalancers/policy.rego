package block_loadbalancers

violation[{"msg": msg}] {
	input.review.kind.kind == "Service"
	input.review.object.spec.type == "LoadBalancer"
	msg := sprintf("LoadBalancer not permitted on Service - %v", [input.review.object.metadata.name])
}